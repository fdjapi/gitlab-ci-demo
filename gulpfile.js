const gulp = require('gulp');
const stylelint = require('gulp-stylelint');
const eslint = require('gulp-eslint');
const mochaPhantomJS = require('gulp-mocha-phantomjs');

gulp.task('lint-css', () => {
  return gulp.src('./style.css')
    .pipe(stylelint({
      reporters: [
        {formatter: 'string', console: true}
      ]
    }));
});

gulp.task('lint-js', () => {
  return gulp.src('./script.js')
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});

gulp.task('test', () => {
  return gulp.src('test/*.js')
    .pipe(mochaPhantomJS({
      phantomjs: {
        useColors: true,
        settings: {
          webSecurityEnabled: false
        }
      }
    }));
});

gulp.task('default', gulp.parallel('lint-css', 'lint-js', 'test'));
